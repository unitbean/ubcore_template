<%@ page import="BASEPACKAGE.NAME.routes.APPERNAMEAdminRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="modal fade custom-width" id="modal-NAME">
    <div class="modal-dialog" style="width: 96%">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Выбор RUSNAME</h4>
            </div>

            <div class="modal-body">

                <div class="row">

                    <div class="col-lg-5">
                        <div class="input-group">
                            <input type="text" class="form-control input-sm" id="modal-NAME-query" name="query"
                                   value="" placeholder="Поиск"/>

                            <div class="input-group-btn">
                                <button type="submit" id="modal-NAME-search" class="btn btn-sm btn-default"><i
                                        class="entypo-search">Поиск </i>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
                <section id="modal-NAME-parent-content"></section>

            </div>
        </div>
    </div>
</div>

<script>
    function initAPPERNAMEModal() {
        $.get("<%= APPERNAMEAdminRoutes.MODAL_PARENT%>",
                {query: $('#modal-NAME-query').val()},
                function (data) {
                    updateAPPERNAMEContent(data);
                });
    }

    function updateAPPERNAMEContent(data) {
        $('#modal-NAME-parent-content').html(data);
    }

    function onClickAPPERNAMEMTable() {
        var id = $(this).attr('data-id');
        var title = $(this).attr('data-title');
        $('#parent-NAME-hidden').val(id);
        $('#parent-NAME').val(title);
        $('#modal-NAME').modal('hide');
    }

    function onClickAPPERNAMEPaginator() {
        var q = $(this).attr('data-query');
        var n = $(this).attr('data-number');
        $.get("<%= APPERNAMEAdminRoutes.MODAL_PARENT%>",
                {query: q, currentPage: n},
                function (data) {
                    updateAPPERNAMEContent(data);
                });
    }

    $(function () {
        $('#btn_parent_NAME').click(function () {
            $('#modal-NAME').modal('show');
            initAPPERNAMEModal();
            return false;
        });
        $('#btn_parent_NAME_clear').click(function () {
            $('#parent-NAME-hidden').val('');
            $('#parent-NAME').val('');
            return false;
        });

        $('#modal-NAME').on('click', '.modal-NAME-line', onClickAPPERNAMEMTable);
        $('#modal-NAME').on('click', '.modal-NAME-goto', onClickAPPERNAMEPaginator);
        $('#modal-NAME-search').click(initAPPERNAMEModal);

    });
</script>
