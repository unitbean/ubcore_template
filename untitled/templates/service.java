package BASEPACKAGE.NAME.services;

import BASEPACKAGE.NAME.models.APPERNAMEDoc;
import BASEPACKAGE.NAME.events.IAPPERNAMEEvent;
import BASEPACKAGE.NAME.views.all.SearchAPPERNAMEAdminRequest;
import BASEPACKAGE.NAME.views.all.SearchAPPERNAMEAdminResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.bson.types.ObjectId;

import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

@Component
public class APPERNAMEService {
    private static Map<String, IAPPERNAMEEvent> events = new HashMap<String, IAPPERNAMEEvent>();

    @Autowired private MongoTemplate mongoTemplate;

    public static void addEvent(IAPPERNAMEEvent event) {
        events.put(event.getClass().getCanonicalName(), event);
    }

    public APPERNAMEDoc save(APPERNAMEDoc doc) {
        doc.setUpdateAt(new Date());
        mongoTemplate.save(doc);
        callAfterSave(doc);
        return doc;
    }

    public APPERNAMEDoc findById(ObjectId id) {
        return mongoTemplate.findById(id, APPERNAMEDoc.class);
    }

    public void remove(ObjectId id) {
        APPERNAMEDoc doc = findById(id);
        if (doc == null) return;
        mongoTemplate.remove(doc);
        callAfterDelete(doc);
    }

    public Long count(Query query) {
        return mongoTemplate.count(query, APPERNAMEDoc.class);
    }

    public SearchAPPERNAMEAdminResponse findAll(SearchAPPERNAMEAdminRequest request) {
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable = new PageRequest(
                request.getCurrentPage(),
                request.getPageSize(),
                sort);

        Criteria criteria = new Criteria();
        //Criteria.where("title").regex(request.getQuery(), "i");

        Query query = new Query(criteria);
        Long count = mongoTemplate.count(query, APPERNAMEDoc.class);
        query = query.with(pageable);

        List<APPERNAMEDoc> result = mongoTemplate.find(query, APPERNAMEDoc.class);
        SearchAPPERNAMEAdminResponse response = new SearchAPPERNAMEAdminResponse(
                request.getCurrentPage(),
                request.getPageSize(),
                result);
        response.setAll(count);
        response.setQuery(request.getQuery());
        return response;
    }

    private void callAfterSave(APPERNAMEDoc doc) {
        for (IAPPERNAMEEvent event : events.values()) {
            event.afterSave(doc);
        }
    }

    private void callAfterDelete(APPERNAMEDoc doc) {
        for (IAPPERNAMEEvent event : events.values()) {
            event.afterDelete(doc);
        }
    }
}
