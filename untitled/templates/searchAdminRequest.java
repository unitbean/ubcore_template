package BASEPACKAGE.NAME.views.all;

import com.ub.core.base.search.SearchRequest;

public class SearchAPPERNAMEAdminRequest extends SearchRequest {
    public SearchAPPERNAMEAdminRequest() {
    }

    public SearchAPPERNAMEAdminRequest(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public SearchAPPERNAMEAdminRequest(Integer currentPage, Integer pageSize) {
        this.pageSize = pageSize;
        this.currentPage = currentPage;
    }

}
