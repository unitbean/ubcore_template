package BASEPACKAGE.NAME.menu;

import com.ub.core.base.menu.CoreMenu;
import com.ub.core.menu.models.fields.MenuIcons;

public class APPERNAMEMenu extends CoreMenu {
    public APPERNAMEMenu() {
        this.name = "RUSNAME";
        this.icon = MenuIcons.ENTYPO_DOC_TEXT;
    }
}
