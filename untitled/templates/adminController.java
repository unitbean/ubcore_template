package BASEPACKAGE.NAME.controllers;

import com.mongodb.gridfs.GridFSDBFile;
import BASEPACKAGE.NAME.models.APPERNAMEDoc;
import BASEPACKAGE.NAME.routes.APPERNAMEAdminRoutes;
import BASEPACKAGE.NAME.services.APPERNAMEService;
import BASEPACKAGE.NAME.views.all.SearchAPPERNAMEAdminRequest;
import BASEPACKAGE.NAME.views.all.SearchAPPERNAMEAdminResponse;
import com.ub.core.base.utils.RouteUtils;
import com.ub.core.file.services.FileService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.ub.core.base.views.breadcrumbs.BreadcrumbsLink;
import com.ub.core.base.views.pageHeader.PageHeader;

@Controller
public class APPERNAMEAdminController {

    @Autowired private APPERNAMEService NAMEService;

    private PageHeader defaultPageHeader(String current) {
        PageHeader pageHeader = PageHeader.defaultPageHeader();
        pageHeader.setLinkAdd(APPERNAMEAdminRoutes.ADD);
        pageHeader.getBreadcrumbs().getLinks().add(new BreadcrumbsLink(APPERNAMEAdminRoutes.ALL, "Все RUSNAME"));
        pageHeader.getBreadcrumbs().setCurrentPageTitle(current);
        return pageHeader;
    }

    @RequestMapping(value = APPERNAMEAdminRoutes.ADD, method = RequestMethod.GET)
    public String create(Model model) {
        APPERNAMEDoc doc = new APPERNAMEDoc();
        doc.setId(new ObjectId());
        model.addAttribute("doc", doc);
        model.addAttribute("pageHeader", defaultPageHeader("Добавление"));
        return "BASEPACKAGE.admin.NAME.add";
    }

    @RequestMapping(value = APPERNAMEAdminRoutes.ADD, method = RequestMethod.POST)
    public String create(@ModelAttribute("doc") APPERNAMEDoc doc,
                         RedirectAttributes redirectAttributes) {
        NAMEService.save(doc);
        redirectAttributes.addAttribute("id", doc.getId());
        return RouteUtils.redirectTo(APPERNAMEAdminRoutes.EDIT);
    }

    @RequestMapping(value = APPERNAMEAdminRoutes.EDIT, method = RequestMethod.GET)
    public String update(@RequestParam ObjectId id, Model model) {
        APPERNAMEDoc doc = NAMEService.findById(id);
        model.addAttribute("doc", doc);
        model.addAttribute("pageHeader", defaultPageHeader("Редактирование"));
        return "BASEPACKAGE.admin.NAME.edit";
    }

    @RequestMapping(value = APPERNAMEAdminRoutes.EDIT, method = RequestMethod.POST)
    public String update(@ModelAttribute("doc") APPERNAMEDoc doc,
                         RedirectAttributes redirectAttributes) {
        NAMEService.save(doc);
        redirectAttributes.addAttribute("id", doc.getId());
        return RouteUtils.redirectTo(APPERNAMEAdminRoutes.EDIT);
    }

    @RequestMapping(value = APPERNAMEAdminRoutes.ALL, method = RequestMethod.GET)
    public String all(@RequestParam(required = false, defaultValue = "0") Integer currentPage,
                      @RequestParam(required = false, defaultValue = "") String query,
                      Model model) {
        SearchAPPERNAMEAdminRequest searchRequest = new SearchAPPERNAMEAdminRequest(currentPage);
        searchRequest.setQuery(query);
        model.addAttribute("search", NAMEService.findAll(searchRequest));
        model.addAttribute("pageHeader", defaultPageHeader("Все"));
        return "BASEPACKAGE.admin.NAME.all";
    }

    @RequestMapping(value = APPERNAMEAdminRoutes.MODAL_PARENT, method = RequestMethod.GET)
    public String modalResponse(@RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                @RequestParam(required = false, defaultValue = "") String query,
                                Model model) {
        SearchAPPERNAMEAdminRequest searchRequest = new SearchAPPERNAMEAdminRequest(currentPage);
        searchRequest.setQuery(query);
        model.addAttribute("search", NAMEService.findAll(searchRequest));
        return "BASEPACKAGE.admin.NAME.modal.parent";
    }

    @RequestMapping(value = APPERNAMEAdminRoutes.DELETE, method = RequestMethod.GET)
    public String delete(@RequestParam ObjectId id, Model model) {
        model.addAttribute("id", id);
        model.addAttribute("pageHeader", defaultPageHeader("Удаление"));
        return "BASEPACKAGE.admin.NAME.delete";
    }

    @RequestMapping(value = APPERNAMEAdminRoutes.DELETE, method = RequestMethod.POST)
    public String delete(@RequestParam ObjectId id) {
        NAMEService.remove(id);
        return RouteUtils.redirectTo(APPERNAMEAdminRoutes.ALL);
    }
}
