package BASEPACKAGE.NAME.events;

import BASEPACKAGE.NAME.models.APPERNAMEDoc;

public interface IAPPERNAMEEvent {
    public void preSave(APPERNAMEDoc doc);
    public void afterSave(APPERNAMEDoc doc);

    public Boolean preDelete(APPERNAMEDoc doc);
    public void afterDelete(APPERNAMEDoc doc);
}
