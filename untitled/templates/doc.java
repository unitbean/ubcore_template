package BASEPACKAGE.NAME.models;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import com.ub.core.base.models.BaseModel;


import javax.persistence.Id;

@Document
public class APPERNAMEDoc extends BaseModel {
    @Id
    private ObjectId id;
    FIELDS
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }
    GETTERS
}
