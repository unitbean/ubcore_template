package BASEPACKAGE.NAME.menu;

import BASEPACKAGE.NAME.routes.APPERNAMEAdminRoutes;
import com.ub.core.base.menu.CoreMenu;

public class APPERNAMEAllMenu extends CoreMenu {
    public APPERNAMEAllMenu() {
        this.name = "Все";
        this.parent = new APPERNAMEMenu();
        this.url = APPERNAMEAdminRoutes.ALL;
    }
}
