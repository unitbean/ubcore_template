package BASEPACKAGE.NAME.views.all;

import com.ub.core.base.search.SearchResponse;
import BASEPACKAGE.NAME.models.APPERNAMEDoc;

import java.util.List;

public class SearchAPPERNAMEAdminResponse extends SearchResponse {
    private List<APPERNAMEDoc> result;


    public SearchAPPERNAMEAdminResponse() {
    }

    public SearchAPPERNAMEAdminResponse(Integer currentPage, Integer pageSize, List<APPERNAMEDoc> result) {
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.result = result;
    }

    public List<APPERNAMEDoc> getResult() {
        return result;
    }

    public void setResult(List<APPERNAMEDoc> result) {
        this.result = result;
    }
}
