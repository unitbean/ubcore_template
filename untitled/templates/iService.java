package BASEPACKAGE.NAME.services;

import BASEPACKAGE.NAME.models.APPERNAMEDoc;
import org.bson.types.ObjectId;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IAPPERNAMEService extends PagingAndSortingRepository<APPERNAMEDoc, ObjectId> {
}
