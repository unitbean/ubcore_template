package BASEPACKAGE.NAME.menu;

import BASEPACKAGE.NAME.routes.APPERNAMEAdminRoutes;
import com.ub.core.base.menu.CoreMenu;

public class APPERNAMEAddMenu extends CoreMenu {
    public APPERNAMEAddMenu() {
        this.name = "Добавить";
        this.parent = new APPERNAMEMenu();
        this.url = APPERNAMEAdminRoutes.ADD;
    }
}
