package info;

import java.util.ArrayList;
import java.util.List;

public class ClassInfo {
    private String pathToJava;
    private String pathToJsp;
    private String pathToTiles;
    private String classModelName;
    private String classApperCaseModelName;
    private String basePackage;
    private String rusName;
    private String tilesAdminPath;
    private String tilesModuleName;

    private List<PropInfo> propInfos = new ArrayList<PropInfo>();
    public static final String PATH_TO = "path.info";
    public static final String CLASS_FILE = "class.info";

    public ClassInfo() throws Exception{
        propInfos = PropInfo.initProp();
        List<String> classFileLines = FileUtils.readLines(CLASS_FILE);
        List<String> pathLines = FileUtils.readLines(PATH_TO);

        this.pathToJava = pathLines.get(0);
        this.pathToJsp = pathLines.get(1);
        this.pathToTiles = pathLines.get(2);
        this.basePackage = pathLines.get(4);
        this.tilesAdminPath = pathLines.get(3);
        this.tilesModuleName = pathLines.get(5);
        this.classModelName = classFileLines.get(0);
        this.classApperCaseModelName = classFileLines.get(1);
        this.rusName = classFileLines.get(2);

        String test = "";
    }

    public List<PropInfo> getPropInfos() {
        return propInfos;
    }

    public void setPropInfos(List<PropInfo> propInfos) {
        this.propInfos = propInfos;
    }

    public String getPathToJava() {
        return pathToJava;
    }

    public void setPathToJava(String pathToJava) {
        this.pathToJava = pathToJava;
    }

    public String getPathToJsp() {
        return pathToJsp;
    }

    public void setPathToJsp(String pathToJsp) {
        this.pathToJsp = pathToJsp;
    }

    public String getPathToTiles() {
        return pathToTiles;
    }

    public void setPathToTiles(String pathToTiles) {
        this.pathToTiles = pathToTiles;
    }

    public String getClassModelName() {
        return classModelName;
    }

    public void setClassModelName(String classModelName) {
        this.classModelName = classModelName;
    }

    public String getClassApperCaseModelName() {
        return classApperCaseModelName;
    }

    public void setClassApperCaseModelName(String classApperCaseModelName) {
        this.classApperCaseModelName = classApperCaseModelName;
    }

    public String getBasePackage() {
        return basePackage;
    }

    public void setBasePackage(String basePackage) {
        this.basePackage = basePackage;
    }

    public String getRusName() {
        return rusName;
    }

    public void setRusName(String rusName) {
        this.rusName = rusName;
    }

    public String getTilesAdminPath() {
        return tilesAdminPath;
    }

    public void setTilesAdminPath(String tilesAdminPath) {
        this.tilesAdminPath = tilesAdminPath;
    }

    public String getTilesModuleName() {
        return tilesModuleName;
    }

    public void setTilesModuleName(String tilesModuleName) {
        this.tilesModuleName = tilesModuleName;
    }
}
