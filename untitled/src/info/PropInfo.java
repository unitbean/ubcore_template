package info;

import java.util.ArrayList;
import java.util.List;

public class PropInfo {
    private String typeOfProp;
    private String name;
    private String nameApperCase;
    private String rusName;

    public static final String PROP_FILE = "prop.info";

    public static List<PropInfo> initProp() throws Exception {
        List<PropInfo> res = new ArrayList<PropInfo>();
        List<String> content = FileUtils.readLines(PROP_FILE);

        for(String line : content){
            try {
                String[] cvs = line.split(";");
                PropInfo propInfo = new PropInfo();

                propInfo.setTypeOfProp(cvs[0]);
                propInfo.setName(cvs[1]);
                propInfo.setNameApperCase(cvs[2]);
                propInfo.setRusName(cvs[3]);
                res.add(propInfo);
            } catch (Exception e) {
            }
        }

        return res;
    }

    public String getTypeOfProp() {
        return typeOfProp;
    }

    public void setTypeOfProp(String typeOfProp) {
        this.typeOfProp = typeOfProp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameApperCase() {
        return nameApperCase;
    }

    public void setNameApperCase(String nameApperCase) {
        this.nameApperCase = nameApperCase;
    }

    public String getRusName() {
        return rusName;
    }

    public void setRusName(String rusName) {
        this.rusName = rusName;
    }
}
