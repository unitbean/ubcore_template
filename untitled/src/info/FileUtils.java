package info;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {

    public static String read(String filePath) throws Exception {
        StringBuffer fileData = new StringBuffer();
        BufferedReader reader = new BufferedReader(
                new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead = 0;
        while ((numRead = reader.read(buf)) != -1) {
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
        }
        reader.close();
        return fileData.toString();
    }

    public static List<String> readLines(String filePath) throws Exception {
        BufferedReader reader = new BufferedReader(
                new FileReader(filePath));
        List<String> lines = new ArrayList<String>();

        while (true) {
            try {
                String line = reader.readLine();

                if (line == null)
                    break;

                lines.add(line);
            } catch (Exception e) {
                break;
            }
        }

        reader.close();
        return lines;
    }

    public static void write(String fileName, String content) throws Exception {
        File file = new File(fileName);


        // if file doesnt exists, then create it
        if (file.exists()) {
            file.delete();
        }

        File file1 = new File(file.getParent());
        file1.mkdirs();
        file.createNewFile();

        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(content);
        bw.close();
    }

}
