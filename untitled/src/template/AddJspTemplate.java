package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class AddJspTemplate extends Template {

    public AddJspTemplate(ClassInfo classInfo) {
        super(classInfo);
    }
    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates"+File.separator +"addjsp.jsp");
        return content;
    }

    @Override
    public String getFileName() {
        return classInfo.getPathToJsp() + File.separator  + "admin"+File.separator  + classInfo.getClassModelName() + File.separator +"add.jsp";
    }
}
