package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class AdminRoutesTemplate extends Template {
    public AdminRoutesTemplate(ClassInfo classInfo) {
        super(classInfo);
    }

    private String BASEPACKAGE = "BASEPACKAGE";
    private String NAME = "NAME";
    private String APPERNAME = "APPERNAME";

    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates"+File.separator +"adminRoutes.java");

        return content;
    }

    @Override
    public String getFileName() {
        return classInfo.getPathToJava() + File.separator +classInfo.getClassModelName() + File.separator  + "routes" +File.separator  + classInfo.getClassApperCaseModelName() + "AdminRoutes.java";
    }
}
