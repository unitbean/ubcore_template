package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class AddMenuTemplate extends Template {
    public AddMenuTemplate(ClassInfo classInfo) {
        super(classInfo);
    }

    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates"+File.separator +"addMenu.java");
        return content;
    }

    @Override
    public String getFileName() {
        return classInfo.getPathToJava() +  File.separator +classInfo.getClassModelName()+File.separator  + "menu"+File.separator  + classInfo.getClassApperCaseModelName() + "AddMenu.java";
    }
}
