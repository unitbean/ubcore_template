package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class TilesTemplate extends Template {

    public TilesTemplate(ClassInfo classInfo) {
        super(classInfo);
    }

    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates" + File.separator + "tiles.xml");
        return content;
    }

    @Override
    public String getFileName() {

        return classInfo.getPathToTiles() + File.separator + "com-ub-" + classInfo.getTilesModuleName() + "-" + classInfo.getClassModelName() + "-tiles.xml";
    }
}
