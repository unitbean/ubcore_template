package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class EditJspTemplate extends Template {

    public EditJspTemplate(ClassInfo classInfo) {
        super(classInfo);
    }
    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates"+File.separator +"editjsp.jsp");
        return content;
    }

    @Override
    public String getFileName() {
        return classInfo.getPathToJsp() + File.separator  + "admin"+File.separator  + classInfo.getClassModelName() + File.separator +"edit.jsp";
    }
}
