package template;

import info.ClassInfo;
import info.FileUtils;
import info.PropInfo;

public abstract class Template {
    protected ClassInfo classInfo;

    protected String BASEPACKAGE = "BASEPACKAGE";
    protected String NAME = "NAME";
    protected String APPERNAME = "APPERNAME";
    protected String FIELDS = "FIELDS";
    protected String GETTERS = "GETTERS";
    protected String RUSNAME = "RUSNAME";
    protected String FORMFIELDS = "FORMFIELDS";
    protected String TABLEHEADER = "TABLEHEADER";
    protected String TABLEBODY = "TABLEBODY";
    protected String TILESPATH = "TILESPATH";


    public Template(ClassInfo classInfo) {
        this.classInfo = classInfo;
    }

    public abstract String getTemplate() throws Exception;

    public abstract String getFileName();

    public String generateTempalte() throws Exception {
        String content = getTemplate();
        content = content.replaceAll(BASEPACKAGE, classInfo.getBasePackage());
        content = content.replaceAll(APPERNAME, classInfo.getClassApperCaseModelName());
        content = content.replaceAll(RUSNAME, classInfo.getRusName());
        content = content.replaceAll(NAME, classInfo.getClassModelName());
        content = content.replaceAll(TILESPATH, classInfo.getTilesAdminPath());

        //fields
        String fields = "";
        String tableBody = "";
        String tableHeader = "";
        for (PropInfo propInfo : classInfo.getPropInfos()) {
            fields += "private " + propInfo.getTypeOfProp() + " " + propInfo.getName() + ";\n\t";
            tableHeader += "<th>" + propInfo.getRusName() + "</th>";
            tableBody += "<td>\\$\\{doc."+propInfo.getName()+"\\}</td>";
        }

        content = content.replaceAll(TABLEBODY, tableBody);
        content = content.replaceAll(TABLEHEADER, tableHeader);

        //getters
        content = content.replaceAll(GETTERS, GettersSettersTemplate.getTemplate(classInfo.getPropInfos()));

        //formFields
        content = content.replaceAll(FORMFIELDS, FormGenerateTemplate.getTemplate(classInfo.getPropInfos()));
        content = content.replaceAll(FIELDS, fields);
        return content;
    }

    public void save() throws Exception {
        FileUtils.write(getFileName(), generateTempalte());
    }
}
