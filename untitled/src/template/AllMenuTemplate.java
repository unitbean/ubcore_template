package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class AllMenuTemplate extends Template {
    public AllMenuTemplate(ClassInfo classInfo) {
        super(classInfo);
    }

    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates"+ File.separator +"allMenu.java");
        return content;
    }

    @Override
    public String getFileName() {
        return classInfo.getPathToJava() + File.separator +classInfo.getClassModelName()+File.separator  + "menu"+File.separator  + classInfo.getClassApperCaseModelName() + "AllMenu.java";
    }
}
