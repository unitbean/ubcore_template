package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class RoutesTemplate extends Template {
    public RoutesTemplate(ClassInfo classInfo) {
        super(classInfo);
    }

    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates"+File.separator +"routes.java");

        return content;
    }

    @Override
    public String getFileName() {
        return classInfo.getPathToJava() + File.separator +classInfo.getClassModelName()+ File.separator  + "routes"+File.separator  + classInfo.getClassApperCaseModelName() + "Routes.java";
    }
}
