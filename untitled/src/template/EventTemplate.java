package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class EventTemplate extends Template {
    public EventTemplate(ClassInfo classInfo) {
        super(classInfo);
    }

    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates"+ File.separator +"event.java");
        return content;
    }

    @Override
    public String getFileName() {
        return classInfo.getPathToJava() +File.separator +classInfo.getClassModelName()+ File.separator  + "events"+File.separator +"I" + classInfo.getClassApperCaseModelName() + "Event.java";
    }
}
