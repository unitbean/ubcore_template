package template;

import info.FileUtils;
import info.PropInfo;

import java.io.File;
import java.util.List;

public class GettersSettersTemplate {
    public static String TYPE = "TYPE";
    public static String PROPAPPER = "PROPAPPER";
    public static String PROP = "PROP";

    public static String getTemplate(List<PropInfo> propInfos)throws Exception{
        String result = "";
        for(PropInfo propInfo : propInfos){
            String content = FileUtils.read("templates"+ File.separator +"geterssetters.java");
            content = content.replaceAll(TYPE, propInfo.getTypeOfProp());
            content = content.replaceAll(PROPAPPER, propInfo.getNameApperCase());
            content = content.replaceAll(PROP, propInfo.getName());
            result+=content;
        }
        return result;
    }
}
