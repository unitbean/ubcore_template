package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class ServiceTemplate extends Template {
    public ServiceTemplate(ClassInfo classInfo) {
        super(classInfo);
    }

    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates" + File.separator + "service.java");
        return content;
    }

    @Override
    public String getFileName() {
        return classInfo.getPathToJava() + File.separator + classInfo.getClassModelName() + File.separator + "services" + File.separator + classInfo.getClassApperCaseModelName() + "Service.java";
    }
}
