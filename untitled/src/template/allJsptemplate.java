package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class allJsptemplate extends Template {

    public allJsptemplate(ClassInfo classInfo) {
        super(classInfo);
    }

    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates" + File.separator + "alljsp.jsp");
        return content;
    }

    @Override
    public String getFileName() {
        return classInfo.getPathToJsp() + File.separator + "admin" + File.separator + classInfo.getClassModelName() + File.separator + "all.jsp";
    }
}
