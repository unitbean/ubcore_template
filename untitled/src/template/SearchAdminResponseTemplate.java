package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class SearchAdminResponseTemplate extends Template {
    public SearchAdminResponseTemplate(ClassInfo classInfo) {
        super(classInfo);
    }

    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates"+File.separator +"searchAdminResponse.java");

        return content;
    }

    @Override
    public String getFileName() {
        return classInfo.getPathToJava() + File.separator +classInfo.getClassModelName()+File.separator  + "views"+File.separator +"all"+File.separator +"Search"+ classInfo.getClassApperCaseModelName() +"AdminResponse.java";
    }
}
