package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class ControllerTemplate extends Template {
    public ControllerTemplate(ClassInfo classInfo) {
        super(classInfo);
    }

    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates" + File.separator + "controller.java");
        return content;
    }

    @Override
    public String getFileName() {
        return classInfo.getPathToJava() + File.separator + classInfo.getClassModelName() + File.separator + "controllers" + File.separator + classInfo.getClassApperCaseModelName() + "ClientController.java";
    }
}
