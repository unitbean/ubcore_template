package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class IServiceTemplate extends Template {
    public IServiceTemplate(ClassInfo classInfo) {
        super(classInfo);
    }

    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates" + File.separator + "iService.java");
        return content;
    }

    @Override
    public String getFileName() {
        return classInfo.getPathToJava() + File.separator + classInfo.getClassModelName() + File.separator + "services" + File.separator + "I" + classInfo.getClassApperCaseModelName() + "Service.java";
    }
}
