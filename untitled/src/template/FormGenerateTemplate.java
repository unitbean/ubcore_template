package template;

import info.FileUtils;
import info.PropInfo;

import java.io.File;
import java.util.List;

public class FormGenerateTemplate {
    public static String NAME = "NAME";
    public static String RUSNAME = "RUSNAME";

    public static String getTemplate(List<PropInfo> propInfos)throws Exception{
        String result = "";
        for(PropInfo propInfo : propInfos){
            String content = FileUtils.read("templates"+ File.separator +"formEach.jsp");
            content = content.replaceAll(RUSNAME, propInfo.getRusName());
            content = content.replaceAll(NAME, propInfo.getName());
            result+=content;
        }
        return result;
    }
}
