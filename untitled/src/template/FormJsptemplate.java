package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class FormJsptemplate extends Template {

    public FormJsptemplate(ClassInfo classInfo) {
        super(classInfo);
    }

    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates"+File.separator +"formjsp.jsp");
        return content;
    }

    @Override
    public String getFileName() {
        return classInfo.getPathToJsp() + File.separator + "admin" + File.separator + classInfo.getClassModelName() + File.separator + "form.jsp";
    }
}
