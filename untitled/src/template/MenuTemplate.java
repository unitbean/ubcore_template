package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class MenuTemplate extends Template {
    public MenuTemplate(ClassInfo classInfo) {
        super(classInfo);
    }

    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates"+File.separator +"menu.java");
        return content;
    }

    @Override
    public String getFileName() {
        return classInfo.getPathToJava() + File.separator +classInfo.getClassModelName()+ File.separator  + "menu"+File.separator  + classInfo.getClassApperCaseModelName() + "Menu.java";
    }
}
