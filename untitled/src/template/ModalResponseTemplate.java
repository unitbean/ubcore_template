package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class ModalResponseTemplate extends Template {

    public ModalResponseTemplate(ClassInfo classInfo) {
        super(classInfo);
    }

    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates" + File.separator + "modalResponsejsp.jsp");
        return content;
    }

    @Override
    public String getFileName() {
        return classInfo.getPathToJsp() + File.separator + "admin" + File.separator + classInfo.getClassModelName() + File.separator + "modalResponse.jsp";
    }
}
