package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class AdminControllerTemplate extends Template {
    public AdminControllerTemplate(ClassInfo classInfo1) {
        super(classInfo1);
    }

    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates" + File.separator + "adminController.java");
        return content;
    }

    @Override
    public String getFileName() {
        return classInfo.getPathToJava() + File.separator + classInfo.getClassModelName() + File.separator + "controllers" + File.separator + classInfo.getClassApperCaseModelName() + "AdminController.java";
    }

}
