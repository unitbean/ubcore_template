package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class DocTemplate extends Template {
    public DocTemplate(ClassInfo classInfo) {
        super(classInfo);
    }


    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates" + File.separator + "doc.java");
        return content;
    }

    @Override
    public String getFileName() {
        return classInfo.getPathToJava() + File.separator + classInfo.getClassModelName() + File.separator + "models" + File.separator + classInfo.getClassApperCaseModelName() + "Doc.java";
    }
}
