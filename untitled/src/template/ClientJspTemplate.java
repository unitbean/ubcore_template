package template;

import info.ClassInfo;
import info.FileUtils;

import java.io.File;

public class ClientJspTemplate extends Template {

    public ClientJspTemplate(ClassInfo classInfo) {
        super(classInfo);
    }

    @Override
    public String getTemplate() throws Exception {
        String content = FileUtils.read("templates"+File.separator +"client.temp");
        return content;
    }

    @Override
    public String getFileName() {
        return classInfo.getPathToJsp() + File.separator  + "client"+File.separator  + classInfo.getClassModelName() + File.separator  + classInfo.getClassModelName() + ".jsp";
    }
}
