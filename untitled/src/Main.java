import info.ClassInfo;
import template.*;

public class Main {

    public static void main(String[] args) throws Exception {
        ClassInfo classInfo = new ClassInfo();
        if (false) return;
        AdminRoutesTemplate adminRoutesTemplate = new AdminRoutesTemplate(classInfo);
        adminRoutesTemplate.save();

        RoutesTemplate routesTemplate = new RoutesTemplate(classInfo);
        routesTemplate.save();

        DocTemplate docTemplate = new DocTemplate(classInfo);
        docTemplate.save();

//        IServiceTemplate iServiceTemplate = new IServiceTemplate(classInfo);
//        iServiceTemplate.save();

        ServiceTemplate serviceTemplate = new ServiceTemplate(classInfo);
        serviceTemplate.save();

        MenuTemplate menuTemplate = new MenuTemplate(classInfo);
        menuTemplate.save();

        AllMenuTemplate allMenuTemplate = new AllMenuTemplate(classInfo);
        allMenuTemplate.save();

        AddMenuTemplate addMenuTemplate = new AddMenuTemplate(classInfo);
        addMenuTemplate.save();

        FormJsptemplate formJsptemplate = new FormJsptemplate(classInfo);
        formJsptemplate.save();

        AddJspTemplate addJspTemplate = new AddJspTemplate(classInfo);
        addJspTemplate.save();

        allJsptemplate allJsptemplate = new allJsptemplate(classInfo);
        allJsptemplate.save();

        EditJspTemplate editJspTemplate = new EditJspTemplate(classInfo);
        editJspTemplate.save();

        TilesTemplate tilesTemplate = new TilesTemplate(classInfo);
        tilesTemplate.save();

        AdminControllerTemplate adminControllerTemplate = new AdminControllerTemplate(classInfo);
        adminControllerTemplate.save();

        ControllerTemplate controllerTemplate = new ControllerTemplate(classInfo);
        controllerTemplate.save();

        DeletespTemplate deletespTemplate = new DeletespTemplate(classInfo);
        deletespTemplate.save();

        SearchAdminRequestTemplate searchAdminRequestTemplate = new SearchAdminRequestTemplate(classInfo);
        searchAdminRequestTemplate.save();

        SearchAdminResponseTemplate searchAdminResponseTemplate = new SearchAdminResponseTemplate(classInfo);
        searchAdminResponseTemplate.save();

        EventTemplate eventTemplate = new EventTemplate(classInfo);
        eventTemplate.save();

        ModalTemplate modalTemplate = new ModalTemplate(classInfo);
        modalTemplate.save();

        ModalResponseTemplate modalResponseTemplate = new ModalResponseTemplate(classInfo);
        modalResponseTemplate.save();
    }
}
